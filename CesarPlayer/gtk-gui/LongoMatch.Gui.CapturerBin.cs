// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

namespace LongoMatch.Gui {
    
    
    public partial class CapturerBin {
        
        private Gtk.VBox vbox1;
        
        private Gtk.HBox capturerhbox;
        
        private Gtk.DrawingArea logodrawingarea;
        
        private Gtk.HBox hbox2;
        
        private Gtk.HBox buttonsbox;
        
        private Gtk.Button recbutton;
        
        private Gtk.Button pausebutton;
        
        private Gtk.Button stopbutton;
        
        private Gtk.Label timelabel;
        
        protected virtual void Build() {
            Stetic.Gui.Initialize(this);
            // Widget LongoMatch.Gui.CapturerBin
            Stetic.BinContainer.Attach(this);
            this.Name = "LongoMatch.Gui.CapturerBin";
            // Container child LongoMatch.Gui.CapturerBin.Gtk.Container+ContainerChild
            this.vbox1 = new Gtk.VBox();
            this.vbox1.Name = "vbox1";
            this.vbox1.Spacing = 6;
            // Container child vbox1.Gtk.Box+BoxChild
            this.capturerhbox = new Gtk.HBox();
            this.capturerhbox.Name = "capturerhbox";
            this.capturerhbox.Spacing = 6;
            this.vbox1.Add(this.capturerhbox);
            Gtk.Box.BoxChild w1 = ((Gtk.Box.BoxChild)(this.vbox1[this.capturerhbox]));
            w1.Position = 0;
            // Container child vbox1.Gtk.Box+BoxChild
            this.logodrawingarea = new Gtk.DrawingArea();
            this.logodrawingarea.Name = "logodrawingarea";
            this.vbox1.Add(this.logodrawingarea);
            Gtk.Box.BoxChild w2 = ((Gtk.Box.BoxChild)(this.vbox1[this.logodrawingarea]));
            w2.Position = 1;
            // Container child vbox1.Gtk.Box+BoxChild
            this.hbox2 = new Gtk.HBox();
            this.hbox2.Name = "hbox2";
            this.hbox2.Spacing = 6;
            // Container child hbox2.Gtk.Box+BoxChild
            this.buttonsbox = new Gtk.HBox();
            this.buttonsbox.Name = "buttonsbox";
            this.buttonsbox.Spacing = 6;
            // Container child buttonsbox.Gtk.Box+BoxChild
            this.recbutton = new Gtk.Button();
            this.recbutton.TooltipMarkup = "Start or continue capture";
            this.recbutton.Name = "recbutton";
            this.recbutton.UseUnderline = true;
            // Container child recbutton.Gtk.Container+ContainerChild
            Gtk.Alignment w3 = new Gtk.Alignment(0.5F, 0.5F, 0F, 0F);
            // Container child GtkAlignment.Gtk.Container+ContainerChild
            Gtk.HBox w4 = new Gtk.HBox();
            w4.Spacing = 2;
            // Container child GtkHBox.Gtk.Container+ContainerChild
            Gtk.Image w5 = new Gtk.Image();
            w5.Pixbuf = Stetic.IconLoader.LoadIcon(this, "gtk-media-record", Gtk.IconSize.Dialog, 48);
            w4.Add(w5);
            // Container child GtkHBox.Gtk.Container+ContainerChild
            Gtk.Label w7 = new Gtk.Label();
            w4.Add(w7);
            w3.Add(w4);
            this.recbutton.Add(w3);
            this.buttonsbox.Add(this.recbutton);
            Gtk.Box.BoxChild w11 = ((Gtk.Box.BoxChild)(this.buttonsbox[this.recbutton]));
            w11.Position = 0;
            w11.Expand = false;
            w11.Fill = false;
            // Container child buttonsbox.Gtk.Box+BoxChild
            this.pausebutton = new Gtk.Button();
            this.pausebutton.TooltipMarkup = "Pause capture";
            this.pausebutton.Name = "pausebutton";
            this.pausebutton.UseUnderline = true;
            // Container child pausebutton.Gtk.Container+ContainerChild
            Gtk.Alignment w12 = new Gtk.Alignment(0.5F, 0.5F, 0F, 0F);
            // Container child GtkAlignment.Gtk.Container+ContainerChild
            Gtk.HBox w13 = new Gtk.HBox();
            w13.Spacing = 2;
            // Container child GtkHBox.Gtk.Container+ContainerChild
            Gtk.Image w14 = new Gtk.Image();
            w14.Pixbuf = Stetic.IconLoader.LoadIcon(this, "gtk-media-pause", Gtk.IconSize.Dialog, 48);
            w13.Add(w14);
            // Container child GtkHBox.Gtk.Container+ContainerChild
            Gtk.Label w16 = new Gtk.Label();
            w13.Add(w16);
            w12.Add(w13);
            this.pausebutton.Add(w12);
            this.buttonsbox.Add(this.pausebutton);
            Gtk.Box.BoxChild w20 = ((Gtk.Box.BoxChild)(this.buttonsbox[this.pausebutton]));
            w20.Position = 1;
            w20.Expand = false;
            w20.Fill = false;
            // Container child buttonsbox.Gtk.Box+BoxChild
            this.stopbutton = new Gtk.Button();
            this.stopbutton.TooltipMarkup = "Stop and close capture";
            this.stopbutton.Name = "stopbutton";
            this.stopbutton.UseUnderline = true;
            // Container child stopbutton.Gtk.Container+ContainerChild
            Gtk.Alignment w21 = new Gtk.Alignment(0.5F, 0.5F, 0F, 0F);
            // Container child GtkAlignment.Gtk.Container+ContainerChild
            Gtk.HBox w22 = new Gtk.HBox();
            w22.Spacing = 2;
            // Container child GtkHBox.Gtk.Container+ContainerChild
            Gtk.Image w23 = new Gtk.Image();
            w23.Pixbuf = Stetic.IconLoader.LoadIcon(this, "gtk-media-stop", Gtk.IconSize.Dialog, 48);
            w22.Add(w23);
            // Container child GtkHBox.Gtk.Container+ContainerChild
            Gtk.Label w25 = new Gtk.Label();
            w22.Add(w25);
            w21.Add(w22);
            this.stopbutton.Add(w21);
            this.buttonsbox.Add(this.stopbutton);
            Gtk.Box.BoxChild w29 = ((Gtk.Box.BoxChild)(this.buttonsbox[this.stopbutton]));
            w29.Position = 2;
            w29.Expand = false;
            w29.Fill = false;
            this.hbox2.Add(this.buttonsbox);
            Gtk.Box.BoxChild w30 = ((Gtk.Box.BoxChild)(this.hbox2[this.buttonsbox]));
            w30.Position = 0;
            w30.Expand = false;
            w30.Fill = false;
            // Container child hbox2.Gtk.Box+BoxChild
            this.timelabel = new Gtk.Label();
            this.timelabel.Name = "timelabel";
            this.timelabel.Xalign = 1F;
            this.timelabel.LabelProp = "Time: 0:00:00";
            this.hbox2.Add(this.timelabel);
            Gtk.Box.BoxChild w31 = ((Gtk.Box.BoxChild)(this.hbox2[this.timelabel]));
            w31.PackType = ((Gtk.PackType)(1));
            w31.Position = 1;
            w31.Expand = false;
            this.vbox1.Add(this.hbox2);
            Gtk.Box.BoxChild w32 = ((Gtk.Box.BoxChild)(this.vbox1[this.hbox2]));
            w32.Position = 2;
            w32.Expand = false;
            w32.Fill = false;
            this.Add(this.vbox1);
            if ((this.Child != null)) {
                this.Child.ShowAll();
            }
            this.pausebutton.Hide();
            this.stopbutton.Hide();
            this.Show();
            this.logodrawingarea.ExposeEvent += new Gtk.ExposeEventHandler(this.OnLogodrawingareaExposeEvent);
            this.recbutton.Clicked += new System.EventHandler(this.OnRecbuttonClicked);
            this.pausebutton.Clicked += new System.EventHandler(this.OnPausebuttonClicked);
            this.stopbutton.Clicked += new System.EventHandler(this.OnStopbuttonClicked);
        }
    }
}
